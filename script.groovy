#!/user/bin/env groovy


def buildJar() {
    echo "Building JAR file"
    sh 'mvn clean package'
}


def buildImage() {
    echo "Building image"
    withCredentials([usernamePassword(credentialsId: 'nexus', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t 159.65.81.154:8083/jma:1.2 .'
        sh 'echo $PASS | docker login -u $USER --password-stdin 159.65.81.154:8083'
        sh 'docker push 159.65.81.154:8083/jma:1.2'
        sh 'docker tag 159.65.81.154:8083/jma:1.2 159.65.81.154:8083/jma:latest'
        sh 'docker push 159.65.81.154:8083/jma:latest'
    }
}

def deploy() {
    echo "Deploying an application"
    
}

def pushPomFile() {
     withCredentials([usernamePassword(credentialsId: 'gitlab', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh 'git status'
                        sh 'git branch'
                        sh 'git config --list'
                        
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/_alucard/maven-proj.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump pom.xml"'
                        sh 'git push origin HEAD:main'

                        

}
}


return this
