#!/user/bin/env bash

export IMAGE=$1
export DOCKER_USER=$2
export DOCKER_PWD=$3
echo $DOCKER_PWD | docker login -u $DOCKER_USER --password-stdin 159.65.81.154:8083
docker-compose -f docker-compose.yaml up --detach
echo "SUCCESS! LISTEN PORT 8080"
